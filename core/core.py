import os 
import json
import psycopg2

con = psycopg2.connect(
  database="mak", 
  user="postgres", 
  password="pass", 
  host="127.0.0.1", 
  port="5432"
)
print('Success connect to db')

def static_messages():
    with open('core/static/static_messages.json') as f:
        messages_store = json.load(f)
        return messages_store

#First add user 
def add_user(message):
    username = message.from_user.username
    status = False 
    fullName = 'None'
    cur = con.cursor()
    cur.execute("INSERT INTO users (username, fullname, status) VALUES ('{}', '{}', {})".format(username, fullName, status))
    con.commit()  
    print('Success add user')

#Check if user is auth
def check_user_status(message):
    username = message.from_user.username
    cur = con.cursor()
    cur.execute("SELECT status from users WHERE username = '{}'".format(username))
    rows = cur.fetchall()
    for row in rows:
        status = row[0]
    return status

#auth user
def auth_user(message):
    username = message.from_user.username
    full_name = message.text
    status = True
    cur = con.cursor()
    cur.execute("UPDATE users set fullname = '{}', status = {} where username = '{}'".format(full_name, status, username))  
    con.commit()  
    print('Success auth user')

#user_appointments 
def user_appointments(message): 
    username = message.from_user.username
    cur = con.cursor()
    cur.execute("SELECT id FROM users WHERE username = '{}' ".format(username))
    rows = cur.fetchall()
    for row in rows: 
        user_id = row[0]
    cur.execute("SELECT user_appointments.Appointment_id, users.id FROM user_appointments, users WHERE user_appointments.User_id = users.id ")
    app_ids = cur.fetchall()
    list_of_appointment_ids = []
    for app_id in app_ids: 
        list_of_appointment_ids.append(app_id[0])
    if not list_of_appointment_ids:
        return False
    else: 
        return list_of_appointment_ids