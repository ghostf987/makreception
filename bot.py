import telebot
import core.core
from decouple import config

SUPPORTED_COMMANDS = ['Записаться','Мои записи']

NEW_APPOINTMENT = SUPPORTED_COMMANDS[0]
APPOINTMENTS_LIST = SUPPORTED_COMMANDS[1]

bot = telebot.TeleBot(config('API_TOKEN'))

messages = core.core.static_messages()

start_keyboard = telebot.types.ReplyKeyboardMarkup(True, True)
start_keyboard.row(NEW_APPOINTMENT, APPOINTMENTS_LIST)

#Start using bot
@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id, messages['hello'])
    core.core.add_user(message)

#Any text handler
@bot.message_handler(content_types=['text'])
def handler(message):
    is_user_auth = core.core.check_user_status(message)
    if is_user_auth == True:
        if message.text in SUPPORTED_COMMANDS:
            if message.text == 'Записаться':
                create_new_appointment(message)
            elif message.text == 'Мои записи':
                result = core.core.user_appointments(message)
                if result == False:
                    bot.send_message(message.chat.id, messages['empty_appointments'])
                else:
                    pass
        else:
            bot.send_message(message.chat.id, messages['error_message'], reply_markup=start_keyboard)
    else:
        if " " in message.text:
            bot.send_message(message.chat.id, messages['set_name'], reply_markup=start_keyboard)
            core.core.auth_user(message)
        else:  
            bot.send_message(message.chat.id, messages['enter_name'])

def create_new_appointment(message):
    pass



bot.polling()